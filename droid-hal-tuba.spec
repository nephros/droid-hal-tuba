# These and other macros are documented in dhd/droid-hal-device.inc
# Feel free to cleanup this file by removing comments, once you have memorised them ;)

%define device tuba
#define lunch_device lineage_tuba
%define vendor sony

%define vendor_pretty Sony
%define device_pretty Xperia XA

%define droid_target_aarch64 1

%define installable_zip 1

%define android_config \
#define MALI_QUIRKS 1\
%{nil}

%define makefstab_skip_entries / /recovery rootfs 

%define straggler_files \
   /bugreports\
   /cache\
   /d\
   /sdcard\
   /vendor\
   /file_contexts.bin\
   /property_contexts\
   /selinux_version\
   /service_contexts\
   %{nil}
# these are in 15.1 but not in 14.1:
#   /nonplat_file_contexts\
#   /nonplat_hwservice_contexts\
#   /nonplat_property_contexts\
#   /nonplat_seapp_contexts\
#   /nonplat_service_contexts\
#   /plat_file_contexts\
#   /plat_hwservice_contexts\
#   /plat_property_contexts\
#   /plat_seapp_contexts\
#   /plat_service_contexts\
#   /vndservice_contexts\

%include rpm/dhd/droid-hal-device.inc

# IMPORTANT if you want to comment out any macros in your .spec, delete the %
# sign, otherwise they will remain defined! E.g.:
#define some_macro "I'll not be defined because I don't have % in front"

